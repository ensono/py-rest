import base64
import requests
import RestExceptions

class REST(object):
	"""
	This class defines a wrapper for the requests module.
	
	It takes in:
		a base URL for the REST API
		an optional personal access token for authentication or
		an optional user name and password for basic authentication
	"""

	def __init__(self, input_base_url = '', input_personal_access_token = '',
		username = '', password = ''):
		if (not input_base_url):
			raise RestExceptions.RestError("ERROR: no base URL provided")
		self._base_url = input_base_url
		self._personal_access_token = input_personal_access_token
		self._username = username
		self._password = password
		self._encoded_authorization_string = ''
		if (self._username and self._password):
			authorization_string = username + ":" + password
			self._encoded_authorization_string = base64.b64encode(authorization_string)
		if (self._personal_access_token and (self._username or self._password)):
			raise RestExceptions.RestError("ERROR: personal access token and username or password provided")


	def get_authorization_header (self):
		authorization_header = {}
		if (self._personal_access_token):
			authorization_header = {'Authorization': 'Bearer ' + self._personal_access_token}
		else:
			authorization_header = {'Authorization': 'Basic ' + self._encoded_authorization_string}
		return(authorization_header)


	def delete(self, uri):
		url = self._base_url + uri
		headers = self.get_authorization_header()
		try:
			response = requests.delete(url, headers=headers)
		except requests.exceptions.RequestException as e:
			raise RestExceptions.RestError("ERROR: REST::delete", e)
		return(response)


	def get(self, uri, headers = {}, data = {}, params = {}):
		url = self._base_url + uri
		headers.update(self.get_authorization_header())
		try:
			response = requests.get(url, headers=headers, json=data, params=params)
		except requests.exceptions.RequestException as e:
			raise RestExceptions.RestError("ERROR: REST::get", e)
		return(response)


	def post(self, uri, headers = {}, data = {}, files = {}):
		url = self._base_url + uri
		headers.update(self.get_authorization_header())
		try:
			response = requests.post(url, headers=headers, json=data, files=files)
		except requests.exceptions.RequestException as e:
			raise RestExceptions.RestError("ERROR: REST::post", e)
		return(response)


	def put(self, uri, headers = {}, data = {}, params = {}):
		url = self._base_url + uri
		headers.update(self.get_authorization_header())
		try:
			response = requests.put(url, headers=headers, json=data, params=params)
		except requests.exceptions.RequestException as e:
			raise RestExceptions.RestError("ERROR: REST::put", e)
		return(response)


	def __str__(self):
		output = ""
		output += "REST Object:\n"
		output += "------------\n"
		if (self._personal_access_token):
			output += "			 Base URL: " + self._base_url + "\n"
			output += "Personal Access Token: " + self._personal_access_token + "\n"
		else:
			output += " Base URL: " + self._base_url + "\n"
			output += "User Name: " + self._username + "\n"
			output += " Password: " + self._password + "\n"
			output += "Encoded Authorization String: " + self._encoded_authorization_string + "\n"
		return(output)

