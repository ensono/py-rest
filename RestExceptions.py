class RestError(Exception):
    """Generic exception for REST"""
    def __init__(self, message, original_exception = None):
        if (original_exception is not None):
            super(RestError, self).__init__(message + (": %s" % original_exception))
        self._original_exception = original_exception

